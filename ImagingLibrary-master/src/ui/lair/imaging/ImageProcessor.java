package ui.lair.imaging;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import ui.lair.roadClassification.BackProject_Classifier;
import ui.lair.roadClassification.HSV_Classifier;
import ui.lair.roadClassification.RGB_Classifier;
import ui.lair.roadClassification.RGB_HSV_Classifier;
import ui.lair.roadClassification.RoadClassifier;



public class ImageProcessor {
	
	public static final int MaxModels = 20;
	public static final int HISTOGRAM_SIZE = 30;
	
	public final static int RED = 0;
	public final static int GREEN = 1;
	public final static int BLUE = 2;
	public final static int HUE = 0;
	public final static int SAT = 1;
	public final static int MODEL_TYPE_RGB = 0;
	public final static int MODEL_TYPE_HSV = 1;
	public final static int MODEL_TYPE_RGB_HSV = 2;
	public final static int MODEL_TYPE_TEX_BW = 3;
	public final static int MODEL_TYPE_TEX_RGB = 4;
	public final static int MODEL_TYPE_EDGE = 5;
	public final static int MODEL_TYPE_BACKPROJECT = 6;
	public final static int RGB = 0;
	public final static int HSV = 1;
	public final static int GRAY = 2;
	public final static int RGBHSV = 3;
	
	
	public static RoadClassifier getRoadClassifier(int type, int sX, int sY, int eX, int eY){
		
		if(type == MODEL_TYPE_BACKPROJECT){
			return new BackProject_Classifier(sX,sY,eX,eY);
		}
		else if(type == MODEL_TYPE_RGB){
			return new RGB_Classifier(sX,sY,eX,eY);
		}
		else if(type == MODEL_TYPE_HSV){
			return new HSV_Classifier(sX,sY,eX,eY);
		}
		else if(type == MODEL_TYPE_RGB_HSV){
			return new RGB_HSV_Classifier(sX,sY,eX,eY);
		}
		
		
		
		return null;
	}
	
	public static Mat caclulateHistogram(int type, Mat image1, Mat image2){
		List<Mat> imgs = new ArrayList<Mat>();
		imgs.add(image1);
		
		Mat histogram = new Mat();
		
		MatOfInt histSize = null;
        MatOfFloat ranges = null;
        
        if(type == RGB){
        	histSize = new MatOfInt(32,32,32);
        	ranges = new MatOfFloat(0f, 256f,0f, 256f,0f, 256f);
        	
        	Imgproc.calcHist(imgs, new MatOfInt(0,1,2), new Mat(), histogram, histSize, ranges);
        	
            Core.normalize(histogram, histogram);	
            
            
        }
        else if(type == HSV){
        	histSize = new MatOfInt(32,32);
        	ranges = new MatOfFloat(0f, 180f,0f, 256f);
        	
        	Imgproc.calcHist(imgs, new MatOfInt(0,1), new Mat(), histogram, histSize, ranges);
            Core.normalize(histogram, histogram);	
        }
        else if(type == GRAY){
        	histSize = new MatOfInt(32);
        	ranges = new MatOfFloat(0f, 256f);
        	
        	Imgproc.calcHist(imgs, new MatOfInt(0), new Mat(), histogram, histSize, ranges);
            Core.normalize(histogram, histogram);	
        }
        else if(type == RGBHSV){
        	Mat rgbHist = new Mat();
        	histSize = new MatOfInt(32,32,32);
        	ranges = new MatOfFloat(0f, 256f,0f, 256f,0f, 256f);
        	
        	Imgproc.calcHist(imgs, new MatOfInt(0,1,2), new Mat(), rgbHist, histSize, ranges);
            Core.normalize(rgbHist, rgbHist);
            
            imgs.clear();
            imgs.add(image2);
            Mat hsvHist = new Mat();
            histSize = new MatOfInt(32,32);
        	ranges = new MatOfFloat(0f, 180f,0f, 256f);
        	
        	Imgproc.calcHist(imgs, new MatOfInt(0,1), new Mat(), hsvHist, histSize, ranges);
            Core.normalize(hsvHist, hsvHist);	
        	
        	
        	List<Mat> histograms = new ArrayList<Mat>();
    		histograms.add(rgbHist);
    		histograms.add(hsvHist);
    		
    		Mat output = new Mat();
            Core.vconcat(histograms, histogram);
        }
        
       return histogram;
	}
	
	public static List<Mat> split3Channels(Mat input){
		List<Mat> channels = new ArrayList<Mat>();
		channels.add(new Mat());
		channels.add(new Mat());
		channels.add(new Mat());
		
		Core.split(input, channels);
		
		return channels;
	}
	
	public static void equalizeAndMerge(List<Mat> channels, Mat output){
		Imgproc.equalizeHist(channels.get(0), channels.get(0));
		Imgproc.equalizeHist(channels.get(1), channels.get(1));
		Imgproc.equalizeHist(channels.get(2), channels.get(2));
		Mat tmp = new Mat();
		Core.merge(channels, tmp);
		
		Imgproc.cvtColor(tmp, output, Imgproc.COLOR_YCrCb2RGB);
	}
	
	public static void computeLBP(int type, byte[] image, byte[] tmp, int x, int y){
		int temp_center=0;
		int center;
		
	    temp_center = 0;
		int width = 640;
		center = (int) image[y*width+x];
		
		int offset = 14;
		
		if(type == 1){
			
			temp_center |= (image[ (y-offset)*width+x-offset ] > center) ? (1<<7) : (0<<7);
			temp_center |= (image[ (y-offset)*width+x] > center) ? (1<<6) : (0<<6);
			temp_center |= (image[ (y-offset)*width +x+offset] > center) ? (1<<5) : (0<<5);
			temp_center |= (image[y*width+x+offset] > center) ? (1<<4) : (0<<4);
			temp_center |= (image[(y+offset)*width+x+offset] > center) ? (1<<3) : (0<<3);
			temp_center |= (image[(y+offset)*width+x] > center) ? (1<<2) : (0<<2);
			temp_center |= (image[(y+offset)*width+x-offset] > center) ? (1<<1) : (0<<1);
			temp_center |= (image[y*width+x-offset] > center) ? (1<<0) : (0<<0);
		}
		else if(type == 2){
			offset = 16;
			temp_center |= (image[ (y-offset)*width+x ] > center) ? (1<<7) : (0<<7);
			temp_center |= (image[ (y-offset/2)*width+x+offset/2] > center) ? (1<<6) : (0<<6);
			temp_center |= (image[ (y-0)*width +x+offset] > center) ? (1<<5) : (0<<5);
			temp_center |= (image[(y+offset/2)*width+x+offset/2] > center) ? (1<<4) : (0<<4);
			temp_center |= (image[(y+offset)*width+x] > center) ? (1<<3) : (0<<3);
			temp_center |= (image[(y+offset/2)*width+x-offset/2] > center) ? (1<<2) : (0<<2);
			temp_center |= (image[(y)*width+x-offset] > center) ? (1<<1) : (0<<1);
			temp_center |= (image[(y-offset/2)*width+x-offset/2] > center) ? (1<<0) : (0<<0);
		}
		else if(type == 3){
			offset=20;
			
			temp_center |= (image[y*width+x+offset] > center) ? (1<<7) : (0<<7);
			temp_center |= (image[(y+offset)*width+x+offset] > center) ? (1<<6) : (0<<6);
			temp_center |= (image[(y+offset)*width+x] > center) ? (1<<5) : (0<<5);
			temp_center |= (image[(y+offset)*width+x-offset] > center) ? (1<<4) : (0<<4);
			temp_center |= (image[y*width+x-offset] > center) ? (1<<3) : (0<<3);
			temp_center |= (image[ (y-offset)*width+x-offset ] > center) ? (1<<2) : (0<<2);
			temp_center |= (image[ (y-offset)*width+x] > center) ? (1<<1) : (0<<1);
			temp_center |= (image[ (y-offset)*width +x+offset] > center) ? (1<<0) : (0<<0);
		}
		
		tmp[y*width + x]= (byte)temp_center;
	}
	
	public static void computeGRAY_LBP(Mat input){
		
		int size = (int) input.total() * input.channels();
		
		byte[] buff= new byte[size];
		byte[] buff2 = new byte[size];
		
		
		input.get(0, 0, buff);
		input.get(0, 0,buff2);

		for(int x = 1; x < input.cols()-1; x++){				
			for(int y = 1; y < input.rows()-1 ; y++){	
				computeLBP(1,buff,buff2,x,y);
				
			}
		}
		
		input.put(0, 0, buff2);
	}
	
	public static void computeRGB_LBP(Mat input){
		List<Mat> matRGBChannels = new ArrayList<Mat>();
		matRGBChannels.add(new Mat());  //red histogram
		matRGBChannels.add(new Mat());  //green histogram
		matRGBChannels.add(new Mat());  //blue histogram
		
		Core.split(input, matRGBChannels);
		
		List<Mat> output = new ArrayList<Mat>();
		output.add(matRGBChannels.get(0).clone());  //red histogram
		output.add(matRGBChannels.get(1).clone());  //green histogram
		output.add(matRGBChannels.get(2).clone());  //blue histogram
		
		
		///////////////////
		int size = (int) matRGBChannels.get(0).total() * matRGBChannels.get(0).channels();
		byte[] redBuff= new byte[size];
		byte[] greenBuff= new byte[size];
		byte[] blueBuff= new byte[size];
		byte[] red= new byte[size];
		byte[] green= new byte[size];
		byte[] blue= new byte[size];
		
		matRGBChannels.get(0).get(0, 0, redBuff);
		matRGBChannels.get(1).get(0, 0, greenBuff);
		matRGBChannels.get(2).get(0, 0, blueBuff);
		matRGBChannels.get(0).get(0, 0, red);
		matRGBChannels.get(1).get(0, 0, green);
		matRGBChannels.get(2).get(0, 0, blue);
		
		
		int offset = 20;
		for(int x = offset; x < input.cols()-offset; x++){				
			for(int y = offset; y < input.rows()-offset ; y++){	
				computeLBP(3,red,redBuff,x,y);
				computeLBP(3,green,greenBuff,x,y);
				computeLBP(3,blue,blueBuff,x,y);
			}
		}
		
		
		output.get(0).put(0, 0, redBuff);
		output.get(1).put(0, 0, greenBuff);
		output.get(2).put(0, 0, blueBuff);
		
		Core.merge(output, input);
	}
	
	public static void computeRGB_LBP_Multi(Mat m_Rgba, Mat tex1, Mat tex2, Mat tex3){
		//Split tex1
		List<Mat> tex1Channels = new ArrayList<Mat>();
		tex1Channels.add(new Mat());  //red histogram
		tex1Channels.add(new Mat());  //green histogram
		tex1Channels.add(new Mat());  //blue histogram
		
		Core.split(tex1, tex1Channels);
		
		List<Mat> tex1Output = new ArrayList<Mat>();
		tex1Output.add(tex1Channels.get(0).clone());  //red histogram
		tex1Output.add(tex1Channels.get(1).clone());  //green histogram
		tex1Output.add(tex1Channels.get(2).clone());  //blue histogram
		
		//Split tex2
		List<Mat> tex2Channels = new ArrayList<Mat>();
		tex2Channels.add(new Mat());  //red histogram
		tex2Channels.add(new Mat());  //green histogram
		tex2Channels.add(new Mat());  //blue histogram
		
		Core.split(tex2, tex2Channels);
		
		List<Mat> tex2Output = new ArrayList<Mat>();
		tex2Output.add(tex2Channels.get(0).clone());  //red histogram
		tex2Output.add(tex2Channels.get(1).clone());  //green histogram
		tex2Output.add(tex2Channels.get(2).clone());  //blue histogram
		
		//Split tex3
		List<Mat> tex3Channels = new ArrayList<Mat>();
		tex3Channels.add(new Mat());  //red histogram
		tex3Channels.add(new Mat());  //green histogram
		tex3Channels.add(new Mat());  //blue histogram
		
		Core.split(tex3, tex3Channels);
		
		List<Mat> tex3Output = new ArrayList<Mat>();
		tex3Output.add(tex3Channels.get(0).clone());  //red histogram
		tex3Output.add(tex3Channels.get(1).clone());  //green histogram
		tex3Output.add(tex3Channels.get(2).clone());  //blue histogram
		
		
		int size = (int) tex1Channels.get(0).total() * tex1Channels.get(0).channels();
		byte[] redBuff1 = new byte[size];
		byte[] greenBuff1 = new byte[size];
		byte[] blueBuff1 = new byte[size];
		byte[] redBuff2 = new byte[size];
		byte[] greenBuff2 = new byte[size];
		byte[] blueBuff2 = new byte[size];
		byte[] redBuff3 = new byte[size];
		byte[] greenBuff3 = new byte[size];
		byte[] blueBuff3 = new byte[size];
		
		byte[] red= new byte[size];
		byte[] green= new byte[size];
		byte[] blue= new byte[size];
		
		tex1Channels.get(0).get(0, 0, redBuff1);
		tex1Channels.get(1).get(0, 0, greenBuff1);
		tex1Channels.get(2).get(0, 0, blueBuff1);
		
		tex2Channels.get(0).get(0, 0, redBuff2);
		tex2Channels.get(1).get(0, 0, greenBuff2);
		tex2Channels.get(2).get(0, 0, blueBuff2);
		
		tex3Channels.get(0).get(0, 0, redBuff3);
		tex3Channels.get(1).get(0, 0, greenBuff3);
		tex3Channels.get(2).get(0, 0, blueBuff3);
		
		tex1Channels.get(0).get(0, 0, red);
		tex1Channels.get(1).get(0, 0, green);
		tex1Channels.get(2).get(0, 0, blue);
		
		int offset = 20;
		for(int x = offset; x < tex1.cols()-offset; x++){				
			for(int y = offset; y < tex1.rows()-offset ; y++){	
				computeLBP(1,red,redBuff1,x,y);
				computeLBP(1,green,greenBuff1,x,y);
				computeLBP(1,blue,blueBuff1,x,y);
				
				computeLBP(2,red,redBuff2,x,y);
				computeLBP(2,green,greenBuff2,x,y);
				computeLBP(2,blue,blueBuff2,x,y);
				
				computeLBP(3,red,redBuff3,x,y);
				computeLBP(3,green,greenBuff3,x,y);
				computeLBP(3,blue,blueBuff3,x,y);
			}
		}
		
		
		tex1Output.get(0).put(0, 0, redBuff1);
		tex1Output.get(1).put(0, 0, greenBuff1);
		tex1Output.get(2).put(0, 0, blueBuff1);
		
		tex2Output.get(0).put(0, 0, redBuff2);
		tex2Output.get(1).put(0, 0, greenBuff2);
		tex2Output.get(2).put(0, 0, blueBuff2);
		
		tex3Output.get(0).put(0, 0, redBuff3);
		tex3Output.get(1).put(0, 0, greenBuff3);
		tex3Output.get(2).put(0, 0, blueBuff3);
		
		Core.merge(tex1Output, tex1);
		Core.merge(tex2Output, tex2);
		Core.merge(tex3Output, tex3);
		
		Core.addWeighted(tex1, .5, tex2, .5, 0, tex2);
		Core.addWeighted(tex2, .5, tex3, .5, 0, tex3);
		
	}
	
	public static void computeEdge(Mat source_image){
		int threshold_ratio = 2;
		int LowerThreshold = 125;
		int UpperThreshold = LowerThreshold * threshold_ratio;
		int grid_columns_number = 20;
		int grid_rows_number = 10;
		
		Mat bw = source_image.clone();
		int bw_maxX= bw.width();
		int bw_maxY= bw.height();
		List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
		Mat mHierarchy = new Mat();
		Imgproc.cvtColor(source_image, bw, Imgproc.COLOR_RGB2GRAY);
		Imgproc.blur(bw, bw, new Size(4,4));

		Imgproc.Canny(bw, bw, LowerThreshold, LowerThreshold * threshold_ratio, 3, true);
		Core.line(bw, new Point(2,2), new Point(bw_maxX-2,2), new Scalar(255, 255, 255), 2 );	//top
		Core.line(bw, new Point(2,bw_maxY-2), new Point(bw_maxX-2,bw_maxY-2), new Scalar(255, 255, 255), 2 ); //bottom
		Core.line(bw, new Point(2,2), new Point(2,bw_maxY-2), new Scalar(255, 255, 255), 2 ); //left
		Core.line(bw, new Point(bw_maxX-2,2), new Point(bw_maxX-2,bw_maxY-2), new Scalar(255, 255, 255), 2 ); //right
		Imgproc.findContours(bw, contours, mHierarchy, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
		
		int grid_width = source_image.cols()/grid_columns_number;
		int grid_height = source_image.rows()/grid_rows_number;
		double u1, u2, l1, l2;
		int old_x = 0, old_y = 0, new_x, new_y;
		int top_left_contour_idx;
		MatOfPoint2f road_contour;	
		for(int x = 0; x < source_image.cols(); x+=grid_width){
			Core.line(source_image, new Point(x,0), new Point(x,source_image.rows()), new Scalar(255, 255, 255),1);	//drawing of vertical lines
			for(int y = 0; y < source_image.rows() ; y+=grid_height){
				Core.line(source_image, new Point(0,y), new Point(source_image.cols(),y), new Scalar(255, 255, 255),1);

				top_left_contour_idx = -5;
				u1 = -1;
				
				old_x = x;
				old_y = y;
				for (int idx = 0; idx < contours.size(); idx++) {
					road_contour = new MatOfPoint2f( contours.get(idx).toArray() );
					
					if(old_x == 0){
						old_x = 5;
					}
					if(old_y == 0){
						old_y = 5;
					}
					u1 = Imgproc.pointPolygonTest(road_contour, new Point(old_x, old_y), false);
					
					if(u1 == 1){
						top_left_contour_idx = idx;
						break;
					}
				}

				if(top_left_contour_idx != -5){
					road_contour = new MatOfPoint2f( contours.get(top_left_contour_idx).toArray() );

					new_x = x+grid_width;
					new_y = y+grid_height;
					if(new_x >= 640){
						new_x = 640-5;			// -4 because we have drawn outer bounding rectangle of width 2 pixel
					}
					if(new_y >= 480){
						new_y = 480-5;
					}
					
					u1 = Imgproc.pointPolygonTest(road_contour, new Point(old_x, old_y), false);
					u2 = Imgproc.pointPolygonTest(road_contour, new Point(new_x, old_y), false);
					l1 = Imgproc.pointPolygonTest(road_contour, new Point(old_x,new_y), false);
					l2 = Imgproc.pointPolygonTest(road_contour, new Point(new_x, new_y), false);
					
					if(u1!=u2 || u2!=l1 || l1!=l2 || l2 != u1){
						Core.putText(source_image, "1", new Point(old_x+(grid_width/2), old_y+(grid_height/2)), 3, 1, new Scalar(255, 255, 255),1);
					}
				}
			}
		}
	}
	
	public static void singleFeatureLBP(int starty, int endy, int startx, int endx, Mat image, Mat roadRegion, List<Mat> roadRegions){
		ImageProcessor.computeRGB_LBP(image);
		roadRegion = image.submat(starty, endy, startx, endx);
		
		roadRegions.add(roadRegion);
	}
	
	public static void multiFeatureLBP(int starty, int endy, int startx, int endx, Mat image, List<Mat> roadRegions){
		
		Mat tex1=image.clone();
		Mat tex2=image.clone();
		Mat tex3=image.clone();
		
		ImageProcessor.computeRGB_LBP_Multi(image,tex1,tex2,tex3);
		
		Core.addWeighted(tex1, 1, tex2, 1, 0, image);
		Core.addWeighted(image, 1, tex3, 1, 0, image);
		
		roadRegions.add(tex1.submat(starty, endy, startx, endx));
		roadRegions.add(tex2.submat(starty, endy, startx, endx));
		roadRegions.add(tex3.submat(starty, endy, startx, endx));
	}
	
	public static void singleFeatureLBP_WithEqualization(int starty, int endy, int startx, int endx, Mat input1, Mat roadRegion, List<Mat> roadRegions){
		Imgproc.cvtColor(input1, input1, Imgproc.COLOR_RGB2YCrCb);
		List<Mat> tex1Channels = ImageProcessor.split3Channels(input1);
		ImageProcessor.equalizeAndMerge(tex1Channels, input1);
		
		ImageProcessor.computeRGB_LBP(input1);
		roadRegion = input1.submat(starty, endy, startx, endx);
		
		roadRegions.add(roadRegion);
	}
	
	public static void multiFeatureLBP_WithEqualization(int starty, int endy, int startx, int endx, Mat input1, List<Mat> roadRegions){
		Mat tex1=input1.clone();
		Mat tex2=input1.clone();
		Mat tex3=input1.clone();
		
	//Equalize input Mats
		Imgproc.cvtColor(input1, tex1, Imgproc.COLOR_RGB2YCrCb);
		Imgproc.cvtColor(input1, tex2, Imgproc.COLOR_RGB2YCrCb);
		Imgproc.cvtColor(input1, tex3, Imgproc.COLOR_RGB2YCrCb);
		
		List<Mat> tex1Channels = ImageProcessor.split3Channels(tex1);
		List<Mat> tex2Channels = ImageProcessor.split3Channels(tex2);
		List<Mat> tex3Channels = ImageProcessor.split3Channels(tex3);
		
		ImageProcessor.equalizeAndMerge(tex1Channels, tex1);
		ImageProcessor.equalizeAndMerge(tex2Channels, tex2);
		ImageProcessor.equalizeAndMerge(tex3Channels, tex3);
		
	//Compute multi LBP
		ImageProcessor.computeRGB_LBP_Multi(input1,tex1,tex2,tex3);
		
		Core.addWeighted(tex1, 1, tex2, 1, 0, input1);
		Core.addWeighted(input1, 1, tex3, 1, 0, input1);
		
	//Add road regions
		roadRegions.add(tex1.submat(starty, endy, startx, endx));
		roadRegions.add(tex2.submat(starty, endy, startx, endx));
		roadRegions.add(tex3.submat(starty, endy, startx, endx));
	}
	
	public static void singleFeatureLBPRGBHSV(int starty, int endy, int startx, int endx, Mat input1,Mat roadRegion1, Mat input2, Mat roadRegion2, List<Mat> roadRegions){
		
		//Get RGBHSV road regions
		roadRegion1 = input1.submat(starty, endy, startx, endx);
			Imgproc.cvtColor(input1, input2, Imgproc.COLOR_RGB2HSV);
			roadRegion2 = input2.submat(starty, endy, startx, endx);
			
			
		//Compute singl LBP
			ImageProcessor.computeRGB_LBP(input1);

		//Add road regions
			roadRegions.add(input1.submat(starty, endy, startx, endx));
			roadRegions.add(roadRegion1);
			roadRegions.add(roadRegion2);
		}
	
	public static void multiFeatureLBPRGBHSV(int starty, int endy, int startx, int endx, Mat input1,Mat roadRegion1, Mat input2, Mat roadRegion2, List<Mat> roadRegions){
		Mat tex1=input1.clone();
		Mat tex2=input1.clone();
		Mat tex3=input1.clone();
		
		//Get RGBHSV road regions
		roadRegion1 = input1.submat(starty, endy, startx, endx);
		Imgproc.cvtColor(input1, input2, Imgproc.COLOR_RGB2HSV);
		roadRegion2 = input2.submat(starty, endy, startx, endx);
		
		//Compute multi LBP
		ImageProcessor.computeRGB_LBP_Multi(input1,tex1,tex2,tex3);
		
		Core.addWeighted(tex1, 1, tex2, 1, 0, input1);
		Core.addWeighted(tex3, 1, input1, 1, 0, input1);
		

		//Add road regions
		roadRegions.add(tex1.submat(starty, endy, startx, endx));
		roadRegions.add(tex2.submat(starty, endy, startx, endx));
		roadRegions.add(tex3.submat(starty, endy, startx, endx));
		roadRegions.add(roadRegion1);
		roadRegions.add(roadRegion2);
	}
	
	public static void singleFeatureLBPRGBHSV_WithEqualization(int starty, int endy, int startx, int endx, Mat input1,Mat roadRegion1, Mat input2, Mat roadRegion2, List<Mat> roadRegions){
		
		//Get RGBHSV road regions
		roadRegion1 = input1.submat(starty, endy, startx, endx);
		Imgproc.cvtColor(input1, input2, Imgproc.COLOR_RGB2HSV);
		roadRegion2 = input2.submat(starty, endy, startx, endx);
		
		Mat tex1 = new Mat();
		Imgproc.cvtColor(input1, tex1, Imgproc.COLOR_RGB2YCrCb);
		List<Mat> tex1Channels = ImageProcessor.split3Channels(tex1);
		ImageProcessor.equalizeAndMerge(tex1Channels, input1);
		
		//Compute singl LBP
		ImageProcessor.computeRGB_LBP(input1);

		//Add road regions
		roadRegions.add(input1.submat(starty, endy, startx, endx));
		roadRegions.add(roadRegion1);
		roadRegions.add(roadRegion2);
	}
	
	public static void multiFeatureLBPRGBHSV_WithEqualization(int starty, int endy, int startx, int endx, Mat input1,Mat roadRegion1, Mat input2, Mat roadRegion2, List<Mat> roadRegions){
		Mat tex1=input1.clone();
		Mat tex2=input1.clone();
		Mat tex3=input1.clone();
		
		//Get RGBHSV road regions
		roadRegion1 = input1.submat(starty, endy, startx, endx);
		Imgproc.cvtColor(input1, input2, Imgproc.COLOR_RGB2HSV);
		roadRegion2 = input2.submat(starty, endy, startx, endx);
		
		//Equalize
		Imgproc.cvtColor(input1, tex1, Imgproc.COLOR_RGB2YCrCb);
		Imgproc.cvtColor(input1, tex2, Imgproc.COLOR_RGB2YCrCb);
		Imgproc.cvtColor(input1, tex3, Imgproc.COLOR_RGB2YCrCb);
		
		List<Mat> tex1Channels = ImageProcessor.split3Channels(tex1);
		List<Mat> tex2Channels = ImageProcessor.split3Channels(tex2);
		List<Mat> tex3Channels = ImageProcessor.split3Channels(tex3);
		
		ImageProcessor.equalizeAndMerge(tex1Channels, tex1);
		ImageProcessor.equalizeAndMerge(tex2Channels, tex2);
		ImageProcessor.equalizeAndMerge(tex3Channels, tex3);
		
		//Compute multi LBP
		ImageProcessor.computeRGB_LBP_Multi(input1,tex1,tex2,tex3);
		
		Core.addWeighted(tex1, 1, tex2, 1, 0, input1);
		Core.addWeighted(tex3, 1, input1, 1, 0, input1);

		//Add road regions
		roadRegions.add(tex1.submat(starty, endy, startx, endx));
		roadRegions.add(tex2.submat(starty, endy, startx, endx));
		roadRegions.add(tex3.submat(starty, endy, startx, endx));
		roadRegions.add(roadRegion1);
		roadRegions.add(roadRegion2);
	}
	
}
