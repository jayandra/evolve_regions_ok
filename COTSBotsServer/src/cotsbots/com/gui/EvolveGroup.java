package cotsbots.com.gui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Checkbox;
import java.awt.Button;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;

import cotsbots.com.server.ClientThread;
import cotsbots.robot.data.EvolutionConditions;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import javax.swing.JCheckBox;

public class EvolveGroup extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldGenerations;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	public EvolutionConditions conditions;
	public int size;
	final Checkbox chkDistributed = new Checkbox("Distributed");
	final Checkbox chkTestCases = new Checkbox("Share TestCases");
	final Checkbox chkOffline = new Checkbox("Offline");
	final JRadioButton rdbtnTestcase = new JRadioButton("1 TestCase");
	final JRadioButton rdbtnOfEach = new JRadioButton("1 of Each Type");
	final JRadioButton rdbtnOfEach_1 = new JRadioButton("3 of Each Type");
	final JRadioButton rdbtnOfEach_2 = new JRadioButton("10 of Each Type");
	
	ArrayList<ClientThread> robots;
	public java.awt.List info;
	
	/**
	 * Create the frame.
	 */
	public EvolveGroup(EvolutionConditions c, int s, ArrayList<ClientThread> r, java.awt.List i) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 607, 398);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		info = i;
		conditions = c;
		size = s;
		robots = r;
		
		
		
		chkDistributed.setBounds(329, 10, 95, 22);
		contentPane.add(chkDistributed);
		
		final JButton btnDone = new JButton("Done");
		
		btnDone.setBounds(10, 228, 89, 23);
		contentPane.add(btnDone);
		
		textFieldGenerations = new JTextField();
		textFieldGenerations.setText("100");
		textFieldGenerations.setBounds(91, 10, 86, 20);
		contentPane.add(textFieldGenerations);
		textFieldGenerations.setColumns(10);
		
		JLabel lblGenerations = new JLabel("Generations");
		lblGenerations.setBounds(10, 10, 71, 14);
		contentPane.add(lblGenerations);
		
		
		chkTestCases.setBounds(329, 38, 145, 22);
		contentPane.add(chkTestCases);
		
		
		rdbtnTestcase.setSelected(true);
		buttonGroup.add(rdbtnTestcase);
		rdbtnTestcase.setBounds(10, 53, 109, 23);
		contentPane.add(rdbtnTestcase);
		
		buttonGroup.add(rdbtnOfEach);
		rdbtnOfEach.setBounds(10, 79, 167, 23);
		contentPane.add(rdbtnOfEach);
		
		buttonGroup.add(rdbtnOfEach_1);
		rdbtnOfEach_1.setBounds(10, 105, 145, 23);
		contentPane.add(rdbtnOfEach_1);
		
		buttonGroup.add(rdbtnOfEach_2);
		rdbtnOfEach_2.setBounds(10, 131, 167, 23);
		contentPane.add(rdbtnOfEach_2);
		
		
		chkOffline.setBounds(329, 66, 124, 27);
		contentPane.add(chkOffline);
		
		
		
		
		btnDone.addActionListener(new ActionListener()  {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				if( chkDistributed.getState() ){
					conditions.distributed = true;
					conditions.numberOfDistBots = size;
				}else{
					conditions.distributed = false;
				}
				if(chkTestCases.getState()){
					conditions.sharedTestcases = true;
				}
				if(chkOffline.getState() ){
					conditions.offline = true;
				}
				
				conditions.maxGenerations = Integer.parseInt(textFieldGenerations.getText());
				if(rdbtnTestcase.isSelected()){
					conditions.startCondition = 0;
				}else if(rdbtnOfEach.isSelected()){
					conditions.startCondition = 1;
				}
				else if(rdbtnOfEach_1.isSelected()){
					conditions.startCondition = 2;
				}
				else if(rdbtnOfEach_2.isSelected()){
					conditions.startCondition = 3;
				}
				Container frame = btnDone.getParent();
				while( !(frame instanceof JFrame) ){
					frame = frame.getParent();
				}
				for (ClientThread r:robots){
					r.sendEvolutionConditions(conditions, r.ID);
				}
				if( conditions.distributed ){
					new distributedManager().start();
				}
				((JFrame)frame).hide();
			}
		});
		
		
	}
	
	public class distributedManager extends Thread{
		public double[][] elites;
		
		public void run(){
			info.add("DM started");
			elites = new double[robots.size()][];
			while(true){
				int numReady = 0;
				for ( ClientThread r:robots ){
					if( r.myData.evoData.hasNewElite ){
						numReady++;
						elites[r.ID] = r.myData.evoData.elite;
					}
				}
				if ( numReady >= robots.size() ){
					info.add("Sending Elites");
					for ( ClientThread r:robots ){
						r.sendElites(elites);
						r.myData.evoData.hasNewElite = false;
					}
				}
			}
		}
		
	}
	
}
