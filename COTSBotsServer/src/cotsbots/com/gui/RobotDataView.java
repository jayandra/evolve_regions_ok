package cotsbots.com.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JTextField;

import cotsbots.com.server.ClientThread;

public class RobotDataView extends JFrame {

	private JPanel contentPane;

	public ArrayList<ClientThread> myRobots;
	public ClientThread currentRobot;
	
	
	
	JButton btnDone = new JButton("Done");
	JButton btnSave = new JButton("Save");
	List listData = new List();
	JLabel lblRobotData = new JLabel("Robot Data");
	JLabel lblRobot = new JLabel("Robot");
	JLabel lblEvolveConditions = new JLabel("Evolve Conditions");
	
	
	

	/**
	 * Create the frame.
	 */
	public RobotDataView(ArrayList<ClientThread> robots) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 601, 450);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		myRobots = robots;
		
		setup();
		listData.add("robots: " + myRobots.size());
		currentRobot = myRobots.get(0);
	}
	
	public void setup(){
		
		btnDone.setBounds(10, 334, 89, 23);
		contentPane.add(btnDone);
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] lines = listData.getItems();
				try {
			          File file = new File("Robot.txt");
			          BufferedWriter output = new BufferedWriter(new FileWriter(file));
			          for ( int i = 0; i < lines.length; i++ ){
			        	  output.write(lines[i] + "\n");
			          }
			          output.close();
			        } catch ( IOException e ) {
			           
			        }
			}
		});
		
		
		btnSave.setBounds(10, 295, 89, 23);
		contentPane.add(btnSave);
		
		
		listData.setBounds(10, 29, 317, 135);
		contentPane.add(listData);
		
		
		lblRobotData.setBounds(30, 11, 125, 14);
		contentPane.add(lblRobotData);
		
		
		String[] robotNames = new String[myRobots.size()];
		for(int i = 0; i < myRobots.size(); i++){
			robotNames[i] = ""+myRobots.get(i).ID;
		}
		
		final JComboBox comboBoxRobots = new JComboBox(robotNames);
		comboBoxRobots.setBounds(372, 29, 46, 20);
		contentPane.add(comboBoxRobots);
		comboBoxRobots.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0){
				int id = Integer.parseInt(comboBoxRobots.getSelectedItem().toString());
				for ( int i = 0; i < myRobots.size(); i++ ){
					if ( id == myRobots.get(i).ID ){
						currentRobot = myRobots.get(i);
					}
				}
			}
		});
		
		lblRobot.setBounds(372, 11, 46, 14);
		contentPane.add(lblRobot);
		
		
		lblEvolveConditions.setBounds(433, 29, 131, 14);
		contentPane.add(lblEvolveConditions);
		
		JButton btnLoadData = new JButton("Load Data");
		btnLoadData.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if( currentRobot != null ){
					listData.removeAll();
					currentRobot.requestUpdate();
					listData.add("Gen     Best     Avg");
					for( int i = 0; i < currentRobot.myData.evoData.bestFitByGeneration.size(); i++ ){
						double best = currentRobot.myData.evoData.bestFitByGeneration.get(i);
						double average = 0;
						if( i < currentRobot.myData.evoData.averageFitByGeneration.size() ){
							average = currentRobot.myData.evoData.averageFitByGeneration.get(i);
						}
						best = Math.floor(best*100)/100;
						average = Math.floor(average*100)/100;
						String line = "";
						line += i + "     " + best + "     " + average;
						listData.add(line);
					}
					listData.add("test cases: " + currentRobot.myData.evoData.numTestCases);
					listData.add("Forward: " + currentRobot.myData.evoData.forward + "   " +
								 "Left: " + currentRobot.myData.evoData.left + "   " + 
								 "Right: " + currentRobot.myData.evoData.right);
					listData.add("Elites Recieved: " + currentRobot.myData.evoData.numRecElites);
				}
			}
		});
		btnLoadData.setBounds(147, 331, 115, 29);
		contentPane.add(btnLoadData);
	}
}
