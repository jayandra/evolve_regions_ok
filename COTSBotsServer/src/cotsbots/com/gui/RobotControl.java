package cotsbots.com.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import cotsbots.robot.data.RobotData.state;
import cotsbots.com.server.ClientThread;
import javax.swing.JComboBox;

public class RobotControl extends JFrame {

	private JPanel contentPane;
	JButton btnForward = new JButton("Forward");
	JButton btnPause = new JButton("Pause");
	JButton btnReverse = new JButton("Reverse");
	JButton btnLeft = new JButton("Left");
	JButton btnRight = new JButton("Right");
	JButton btnClearAllRobot = new JButton("Clear All Robot Data");
	JButton btnClearEvolutionData = new JButton("Clear Evolution Data");
	JButton btnClearTestCases = new JButton("Clear Test Cases");
	JMenuBar menuBar = new JMenuBar();
	JMenu mnRobotState = new JMenu("Robot State");
	
	public ArrayList<ClientThread> myRobots;
	public ClientThread currentRobot;
	private final JMenuItem mntmStart = new JMenuItem("Start");
	private final JMenuItem mntmTrainVision = new JMenuItem("Train Vision");
	private final JMenuItem mntmTrainAnn = new JMenuItem("Train ANN");
	private final JMenuItem mntmAutonomous = new JMenuItem("Autonomous");
	private final JMenuItem mntmTeleoperate = new JMenuItem("Teleoperate");

	public RobotControl(ArrayList<ClientThread> robots) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 670, 513);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		myRobots = robots;
		currentRobot = myRobots.get(0);
		setup();
		
		
	}
	
	public void setup(){
		btnForward.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				currentRobot.drive(1);
			}
		});
		btnForward.setBounds(392, 72, 97, 47);
		contentPane.add(btnForward);
		
		
		btnPause.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				currentRobot.drive(0);
			}
		});
		btnPause.setBounds(406, 118, 73, 47);
		contentPane.add(btnPause);
		btnPause.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				currentRobot.sendPause();
			}
		});
		
	
		btnReverse.setBounds(392, 164, 97, 47);
		contentPane.add(btnReverse);
		btnReverse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				currentRobot.drive(2);
			}
		});
		
		
		
		btnLeft.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				currentRobot.drive(3);
			}
		});
		
		
		btnLeft.setBounds(334, 118, 73, 47);
		contentPane.add(btnLeft);
		
		btnRight.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				currentRobot.drive(4);
			}
		});
		
		
		btnRight.setBounds(478, 118, 83, 47);
		contentPane.add(btnRight);
		
		
		btnClearAllRobot.setBounds(10, 288, 177, 23);
		contentPane.add(btnClearAllRobot);
		
		
		
		btnClearEvolutionData.setBounds(10, 331, 177, 23);
		contentPane.add(btnClearEvolutionData);
		
		
		
		btnClearTestCases.setBounds(10, 311, 177, 23);
		contentPane.add(btnClearTestCases);
		
		
		menuBar.setBounds(2, 0, 97, 21);
		contentPane.add(menuBar);
		
		
		menuBar.add(mnRobotState);
		mnRobotState.add(mntmStart);
		
		mnRobotState.add(mntmTrainVision);
		
		mnRobotState.add(mntmTrainAnn);
		
		mnRobotState.add(mntmAutonomous);
		
		mnRobotState.add(mntmTeleoperate);
		
		mntmStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				currentRobot.sendStateChange(state.CLEAR);
			}
		});
		mntmTrainVision.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				currentRobot.sendStateChange(state.VISIONTRAINING);
			}
		});
		mntmTrainAnn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				currentRobot.sendStateChange(state.ANNTRAINING);
			}
		});
		mntmAutonomous.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				currentRobot.sendStateChange(state.AUTONOMOUS);
			}
		});
		mntmTeleoperate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				currentRobot.sendStateChange(state.TELEOPERATED);
			}
		});
		
		String[] robotNames = new String[myRobots.size()];
		for(int i = 0; i < myRobots.size(); i++){
			robotNames[i] = ""+myRobots.get(i).ID;
		}
		final JComboBox comboBoxRobots = new JComboBox(robotNames);
		comboBoxRobots.setBounds(350, 255, 57, 20);
		contentPane.add(comboBoxRobots);
		
		JButton btnEvolve = new JButton("Evolve");
		btnEvolve.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				currentRobot.sendEvolve();
			}
		});
		btnEvolve.setBounds(379, 345, 115, 29);
		contentPane.add(btnEvolve);
		comboBoxRobots.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0){
				int id = Integer.parseInt(comboBoxRobots.getSelectedItem().toString());
				for ( int i = 0; i < myRobots.size(); i++ ){
					if ( id == myRobots.get(i).ID ){
						currentRobot = myRobots.get(i);
					}
				}
			}
		});
		
		
	}
}
