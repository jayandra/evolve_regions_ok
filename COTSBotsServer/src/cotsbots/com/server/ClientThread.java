package cotsbots.com.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import cotsbots.com.message.Message;
import cotsbots.robot.data.RobotData;
import cotsbots.robot.data.RobotData.state;

public class ClientThread extends Thread{
	
	
	Socket socket;
	ObjectInputStream input;
	ObjectOutputStream output;
	public int ID;
	public RobotData myData;
	public boolean running = true;
	public java.awt.List info;
	
	public ClientThread(Socket s, int robotID, java.awt.List i){
		socket = s;
		ID = robotID;
		myData = new RobotData();
		myData.ID = ID;
		info = i;
		info.add("CT Created");
		try{
			info.add("Creating Streams");
			output = new ObjectOutputStream(socket.getOutputStream());
			input = new ObjectInputStream(socket.getInputStream());
			
			//info.add("Streams Created");
		}catch(Exception e){
			//info.add("Streams Not Created");
		}
		Message m = new Message();
		m.ID = ID;
		m.msgType = m.newConnectionRobot;
		//info.add("Message Created");
		try{
			output.writeObject(m);
			//info.add("Message Sent");
			
		}catch( Exception e){
			//info.add("Message Not Sent");
		}
		
		
	}
	
	public void update(Message m){
		//myData = m.data;
		
		info.add("info recieved");
		myData.ID 							= 	m.data.ID;
		myData.currentState 				=	m.data.currentState;
		myData.evoReady 					= 	m.data.evoReady;
		myData.Conditions.maxGenerations 	= 	m.data.Conditions.maxGenerations;
		myData.Conditions.distributed 		= 	m.data.Conditions.distributed;
		myData.Conditions.numberOfDistBots 	= 	m.data.Conditions.numberOfDistBots;
		myData.Conditions.sharedTestcases 	= 	m.data.Conditions.sharedTestcases;
		myData.Conditions.startCondition 	= 	m.data.Conditions.startCondition;
		myData.evoData.currentAverage 		= 	m.data.evoData.currentAverage;
		myData.evoData.currentBest 			= 	m.data.evoData.currentBest;
		
		myData.evoData.numGenerations 		= 	m.data.evoData.numGenerations;
		myData.evoData.numTestCases			= 	m.data.evoData.numTestCases;
		myData.evoData.averageFitByGeneration = m.data.evoData.averageFitByGeneration;
		myData.evoData.bestFitByGeneration	= 	m.data.evoData.bestFitByGeneration;
		myData.evoData.currentGeneration 	= 	m.data.evoData.currentGeneration;
		myData.evoData.forward				=   m.data.evoData.forward;
		myData.evoData.left					=   m.data.evoData.left;
		myData.evoData.right				=   m.data.evoData.right;
		myData.evoData.numRecElites			= 	m.data.evoData.numRecElites;
		
	}
	
	public void sendPause(){
		Message m = new Message();
		m.ID = ID;
		m.msgType = m.pause;
		sendMessage(m);
	}
	
	public void sendMessage( Message m){
		try {
			output.writeObject(m);
		} catch (IOException e) {
			
		}
	}
	public void drive( int dir ){
		Message m = new Message();
		m.ID = ID;
		m.msgType = m.moveCommand;
		m.direction = dir;
		sendMessage(m);
	}
	
	public void sendEvolutionConditions(cotsbots.robot.data.EvolutionConditions conditions, int ID){
		Message m = new Message();
		m.data.Conditions = conditions;
		m.ID = ID;
		m.msgType = m.setEvolutionConditions;
		sendMessage(m);
	}
	
	public void sendStateChange(state newState){
		Message m = new Message();
		m.ID = ID;
		m.msgType = m.changeState;
		m.data.currentState = newState;
		sendMessage(m);
	}
	public void sendEvolve(){
		Message m = new Message();
		m.ID = ID;
		m.msgType = m.evolve;
		sendMessage(m);
	}
	public void requestUpdate(){
		Message m = new Message();
		m.ID = ID;
		m.msgType = m.updateRequest;
		sendMessage(m);
	}
	public void sendElites(double[][] elites){
		Message m = new Message();
		m.ID = ID;
		m.msgType = m.sendElites;
		m.data.evoData.elitePopulation = elites;
		sendMessage(m);
	}
	
	public void processMessage(Message m){
		switch(m.msgType){
		case 1: //Update Robot
			//myRobot.data; set DriveType
			break;
		case 2: //Receive state change
			break;
		case 3: 
			break;
		case 4: // recieve update
			update(m);
			break;
		case 5:
			break;
		case 6:
			break;
		case 7:
			break;
		case 8: // update elite
			info.add("Robot " + ID + " elite recieved");
			myData.evoData.elite = m.data.evoData.elite;
			myData.evoData.hasNewElite = true;
			break;
		case 9:
			break;
		
		}
		
	}

	
	public void run(){
		while(running){
			try {
				processMessage((Message)input.readObject());
			} catch (Exception e) {
				 
			}
			
		}
	}
	
	
}
