package com.example.networkgenalgorithm;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import nnet.MultiLayerPerceptron;
import util.TransferFunctionType;

import core.Connection;
import core.Layer;
import core.NeuralNetwork;
import core.Neuron;

public class networkGenAlgorithm {
	
	public List<double[]> testCases;// = new List<Double>;
	public List<Integer> ans;
	
	public int popSize;
	public int inLayerSize, hidLayerSize, outLayerSize; 
	
	public Random rng = new Random();
	public double mutationMult = .05;
	public double mutationRate =.25;
	
	public List<NeuralNetwork> population;
	public List<Integer> fitness;
	
	
	public NeuralNetwork best;
	
	networkGenAlgorithm(int pop, int inLay, int hidLay, int outLay){
		popSize = pop;
		inLayerSize = inLay;
		hidLayerSize = hidLay;
		outLayerSize = outLay;
	}
	
	
	public void initialize(){
		for( int i = 0; i < popSize; i++ ){
			population.add( new MultiLayerPerceptron( TransferFunctionType.SIGMOID, inLayerSize, hidLayerSize, outLayerSize ) );
			population.get(i).randomizeWeights();
			fitness.add(0);
		}
		
	}
	
	public void runGeneration(){
		
		List<NeuralNetwork> nextGen = new ArrayList<NeuralNetwork>();
		nextGen.add(copyNetwork(best));
		nextGen.add(copyNetwork(best));
		while( nextGen.size() < population.size() ){
			NeuralNetwork parent1, parent2, offspring;
			parent1 = select();
			parent2 = select();
			offspring = crossover(parent1, parent2);
			mutate(offspring);
			nextGen.add(copyNetwork(offspring));
		}
		population.clear();
		for( int i = 0; i < nextGen.size(); i++ ){
			population.add(copyNetwork(nextGen.get(i)));
		}
		calcFitness();
		findBest();
		
	}
	public NeuralNetwork crossover(NeuralNetwork parent1, NeuralNetwork parent2){
		
		NeuralNetwork offspring = new MultiLayerPerceptron( TransferFunctionType.SIGMOID, inLayerSize, hidLayerSize, outLayerSize );
		List<Connection> offspringconnections = new ArrayList<Connection>();
		List<Connection> parent1connections = new ArrayList<Connection>();
		List<Connection> parent2connections = new ArrayList<Connection>();
		
		for (Layer layer : offspring.getLayers()) {
            for (Neuron neuron : layer.getNeurons()) {

                for (Connection connection : neuron.getInputConnections()) {
                	offspringconnections.add(connection);
                }
            }
        }
		for (Layer layer : parent1.getLayers()) {
            for (Neuron neuron : layer.getNeurons()) {
                for (Connection connection : neuron.getInputConnections()) {
                	parent1connections.add(connection);
                }
            }
        }
		for (Layer layer : parent2.getLayers()) {
            for (Neuron neuron : layer.getNeurons()) {
                for (Connection connection : neuron.getInputConnections()) {
                	parent2connections.add(connection);
                }
            }
        }
		
		
		for( int i = 0; i < offspringconnections.size(); i++ ){
			if( rng.nextBoolean() ){
				offspringconnections.get(i).getWeight().setValue(parent1connections.get(i).getWeight().value);
			}else{
				offspringconnections.get(i).getWeight().setValue(parent2connections.get(i).getWeight().value);
			}
		}

		return offspring;
	}
	
	public void mutate(NeuralNetwork offspring){
		
		for (Layer layer : offspring.getLayers()) {
            for (Neuron neuron : layer.getNeurons()) {
                for (Connection connection : neuron.getInputConnections()) {
                	if ( rng.nextDouble() < mutationRate ){
                		if ( rng.nextBoolean() ){
                			connection.getWeight().inc(connection.getWeight().value * mutationMult);
                		}else{
                			connection.getWeight().dec(connection.getWeight().value * mutationMult);
                		}
                	}
                }
            }
        }
	}
	
	
	public NeuralNetwork select(){
		NeuralNetwork selected;
		int position = -1;
		
		for( int i = 0; i < 3; i++ ){
			if( position == -1 ){
				position = rng.nextInt(popSize);
			}else{
				int challenger = rng.nextInt(popSize);
				if( fitness.get(challenger) > fitness.get(position) ){
					position = challenger;
				}
			}
		}
		
		selected = population.get(position);
		
		return selected;		
	}
	
	public NeuralNetwork copyNetwork( NeuralNetwork inNet ){
		
		NeuralNetwork copiedNet = new MultiLayerPerceptron( TransferFunctionType.SIGMOID, inLayerSize, hidLayerSize, outLayerSize );
		
		List<Connection> innetconnections = new ArrayList<Connection>();
		List<Connection> copiedconnections = new ArrayList<Connection>();
		
		for (Layer layer : inNet.getLayers()) {
            for (Neuron neuron : layer.getNeurons()) {
                for (Connection connection : neuron.getInputConnections()) {
                	innetconnections.add(connection);
                }
            }
        }
		
		for (Layer layer : copiedNet.getLayers()) {
            for (Neuron neuron : layer.getNeurons()) {
                for (Connection connection : neuron.getInputConnections()) {
                	copiedconnections.add(connection);
                }
            }
        }
		
		for( int i = 0; i < copiedconnections.size(); i++){
			copiedconnections.get(i).getWeight().value = innetconnections.get(i).getWeight().value;
		}
		
		
		return copiedNet;
	}
	public void calcFitness(){
		fitness.clear();
		for( int i = 0; i < population.size(); i++){
			int tempFit = 0;
			NeuralNetwork currentNet = population.get(i);
			
			for( int j = 0; j < testCases.size(); j++ ){
				int currentAnswer = -1;
				int testAnswer = ans.get(j);
				currentNet.setInput(testCases.get(j));
				currentNet.calculate();
				double[] currentout = currentNet.getOutput();
				if( currentout[0] > currentout[1] && currentout[0] > currentout[2] ){
					currentAnswer = 0;
				}else if( currentout[1] > currentout[0] && currentout[1] > currentout[2] ){
					currentAnswer = 1;
				}else if( currentout[2] > currentout[0] && currentout[2] > currentout[1] ){
					currentAnswer = 2;
				}else{
					currentAnswer = 3;
				}
				if( currentAnswer == testAnswer ){
					tempFit++;
				}
				
			}
			fitness.set(i, tempFit);
		}
		
		
		
		
		
	}
	
	public void addTestCase( double[] input, double[] output ){
		testCases.add(input);
		if( output[0] > output[1] && output[0] > output[2] ){
			ans.add(0);
		}else if( output[1] > output[0] && output[1] > output[2] ){
			ans.add(1);
		}else if( output[2] > output[0] && output[2] > output[1] ){
			ans.add(2);
		}else{
			ans.add(3);
		}
		
		
	}
	
	public void findBest(){
		int bestPos = -1;
		for ( int i = 0; i < fitness.size(); i++ ){
			if( bestPos == -1 ){
				bestPos = i;
			}else{
				if( fitness.get(i) > fitness.get(bestPos)){
					bestPos = i;
				}
			}
		}
		best = copyNetwork(population.get(bestPos));
	}
	
	public boolean hasBest(){
		if( best != null ){
			return true;
		}
		else{
			return false;
		}
	}
	public NeuralNetwork getBest(){
		return best;
	}

}
