package ui.lair.roadClassification;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import ui.lair.imaging.Model;

import android.util.Log;



public class RoadClassifier implements IModel{
	protected static final int intCompareMethod = Imgproc.CV_COMP_CORREL;
	protected static final double ROAD_MODEL_SIMILARITY_THRESHOLD = .8;
	protected static final double COLOR_MODEL_SIMILARITY_THRESHOLD = 1;
	
	protected int intModelType;
	protected static final int MAX_MODELS = 20;
	protected boolean boolBuildingLibrary;
	
	protected int startX,startY,endX,endY;
	
	public RoadClassifier(int type, int sX, int sY, int eX, int eY){
		intModelType = type;
		boolBuildingLibrary = true;
		
		startX = sX;
		startY = sY;
		endX = eX;
		endY = eY;
	}
	public int getType(){
		
		return intModelType;
	}
	
	public boolean isBuildingLibrary(){
		return boolBuildingLibrary;
	}
	
	public void pauseBuildingLibrary(){
		boolBuildingLibrary = !boolBuildingLibrary;
	}
	
	public double calculateHistogramSimilarity(Model model, Mat regionHistogram){
		if(model == null || model.getHistogram() == null){
			Log.e("RoadClassifier.calculatHistogramSimilarity", "model was null");
			return 0;
		}
		else if(regionHistogram == null){
			Log.e("RoadClassifier.calculatHistogramSimilarity", "region was null");
			return 0;
		}
		else{
			Log.e("RoadClassifier.calculatHistogramSimilarity", model.getHistogram().cols() + "");
			Log.e("RoadClassifier.calculatHistogramSimilarity", regionHistogram.cols() + "");
			return Imgproc.compareHist(model.getHistogram(), regionHistogram, intCompareMethod);
		}
	}
	
	public double[] calc(Mat m_Rgba, int numCols, int numRows){
		
		return null;
	}
	
	
// IModel implementations
	
	@Override
	public void updateModel(Mat cameraImage) {
		// TODO Auto-generated method stub
		
	}
	
	
	
	@Override
	public int getNumberOfModels(){
		
		return 0;
	}

	@Override
	public double calculateRoadProbability(Mat region) {
		
		return 0;
	}
	
	@Override
	public Mat getModelHistogram(int index1, int index2) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void deleteModels() {
		
		
	}
	
	@Override
	public Mat handleSubRegion( Mat input1) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
	
	
	
}
