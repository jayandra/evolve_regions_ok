package ui.lair.imaging;


import org.opencv.core.Mat;

public class Model implements IAge{
	protected double dblAge;
	protected double dblNumberOfSelections;
	
	private Mat histogram;
	
	public Model(Mat hist){
		histogram = hist;
	}
	
	public Mat getHistogram(){
		return histogram;
	}
	public double getAge() {
		return dblAge;
	}
	
	public double getNumberOfSelections() {
		return dblNumberOfSelections;
	}
	
	public void incrementAge() {
		dblAge++;
		
	}
	
	public void incrementNumberOfSelections() {
		dblNumberOfSelections++;
		
	}
	
	public double getSelectionPercentage() {
		return dblNumberOfSelections/dblAge;
	}
}
