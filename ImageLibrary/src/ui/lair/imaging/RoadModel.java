package ui.lair.imaging;


import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Mat;

public class RoadModel implements IAge{
	
	protected double dblAge;
	protected double dblNumberOfSelections;
	
	//private List< ColorModel> colorModels = new ArrayList<ColorModel>();
	private List< Model > roadExamples = new ArrayList<Model>();
	
	/*
	public RoadModel(ColorModel newColorModel){
		
		colorModels.add(newColorModel);
		
		dblAge = 1;
		dblNumberOfSelections = 0;
	}
	
	public ColorModel getColorModel(int index){
		return colorModels.get(index);
	}
	
	public int getSize(){
		return colorModels.size();
	}
	
	public void addColorModel(ColorModel newColorModel){
		colorModels.add(newColorModel);
	}
	
	public void replaceColorModel(int replaceIndex, ColorModel newColorModel){
		colorModels.set(replaceIndex, newColorModel);
	}
	
	*/
	
	public RoadModel(Model newModel){
		
		roadExamples.add(newModel);
		
		dblAge = 1;
		dblNumberOfSelections = 0;
	}
	
	public Model getModel(int index){
		return roadExamples.get(index);
	}
	
	public int getSize(){
		return roadExamples.size();
	}
	
	public void addModel(Model newColorModel){
		roadExamples.add(newColorModel);
	}
	
	public void replaceModel(int replaceIndex, Model newColorModel){
		roadExamples.set(replaceIndex, newColorModel);
	}

	public double getAge() {
		return dblAge;
	}
	
	public double getNumberOfSelections() {
		return dblNumberOfSelections;
	}
	
	public void incrementAge() {
		dblAge++;
		
	}
	
	public void incrementNumberOfSelections() {
		dblNumberOfSelections++;
		
	}
	
	public double getSelectionPercentage() {
		return dblNumberOfSelections/dblAge;
	}
	
	
}
