package cotsbots.robot.robot;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import nnet.MultiLayerPerceptron;
import util.TransferFunctionType;
import android.os.Environment;
import android.util.Log;
import core.Connection;
import core.Layer;
import core.NeuralNetwork;
import core.Neuron;

public class GANetTrainer {
	public int left = 0, right = 0, forward = 0;
	public List<double[]> testCases = new ArrayList<double[]>();// = new List<Double>;
	public List<double[]> ans = new ArrayList<double[]>();
	
	public int popSize;
	public int inLayerSize, hidLayerSize, outLayerSize; 
	
	public Random rng = new Random();
	
	public double mutationRate =.2;
	
	public List<NeuralNetwork> population = new ArrayList<NeuralNetwork>();
	public List<Double> fitness = new ArrayList<Double>();
	
	public List<NeuralNetwork> distPopulation = new ArrayList<NeuralNetwork>();
	public List<Double> distFitness = new ArrayList<Double>();
	
	
	public List<Double> averageFitness = new ArrayList<Double>();
	public List<Double> bestFitness = new ArrayList<Double>();
	public double currentBestFitness;
	public double currentAverageFitness;
	
	public NeuralNetwork best;
	public boolean hasBestArray = false;
	public double[]		 bestArray;
	public double bestError = 1000;
	
	public double fitThresh = .4;
	
	boolean distributed = false;
	boolean distributionStarted = false;
	public double[][] elitePopulation;
	public List<Double> eliteFitness = new ArrayList<Double>();
	public int eliteSize;
	public int elitesRecieved = 0;
	
	public double[][] populationArray;
	public int arraySize;
	
	public int regionPerIndividual = 8;
	public int num_rows = 24;
	public int num_cols = 32;
	public int x_axes[] = new int[] {0, 0, 0, 9, 0, 18, 0, 27, 0, 36, 8, 0, 8, 9, 8, 18, 8, 27, 8, 36, 16, 0, 16, 9, 16, 18, 16, 27, 16, 36, 24, 0, 24, 9, 24, 18, 24, 27, 24, 36, 32, 0, 32, 9, 32, 18, 32, 27, 32, 36, 40, 0, 40, 9, 40, 18, 40, 27, 40, 36, 48, 0, 48, 9, 48, 18, 48, 27, 48, 36, 56, 0, 56, 9, 56, 18, 56, 27, 56, 36};
	public int y_axes[] = new int[] {8, 9, 8, 18, 8, 27, 8, 36, 8, 45, 16, 9, 16, 18, 16, 27, 16, 36, 16, 45, 24, 9, 24, 18, 24, 27, 24, 36, 24, 45, 32, 9, 32, 18, 32, 27, 32, 36, 32, 45, 40, 9, 40, 18, 40, 27, 40, 36, 40, 45, 48, 9, 48, 18, 48, 27, 48, 36, 48, 45, 56, 9, 56, 18, 56, 27, 56, 36, 56, 45, 64, 9, 64, 18, 64, 27, 64, 36, 64, 45};
	
	
	GANetTrainer(int pop, int inLay, int hidLay, int outLay){
		popSize = pop;
		inLayerSize = inLay;
		hidLayerSize = hidLay;
		outLayerSize = outLay;
		
	}
	
	// //////////////////////// - For writing to file in line:779 -	// //////////////////////////////
	public int load_test_cases = 1;

	File storage_path;
	String storage_file = "";

	private File ensure() {
		storage_path = new File(Environment.getExternalStorageDirectory()
				+ "/test_case_write");
		boolean success = true;
		if (!storage_path.exists()) {
			success = storage_path.mkdir();
			if (!success) {
				Log.e("GANetTrainer", "Error creating test_file_write folder");
			}
		}
		return storage_path;
	}

	private void writeToFile(String filename, String content) {
		File file = new File(ensure(), filename);
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (Exception e) {
				e.printStackTrace();
				Log.e("EvolveThread", "Error creating file");
			}
		}
		try {
			FileWriter fw = new FileWriter(file, true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(content);
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
			Log.e("EvolveThread", "Error writing file");
		}
	}

	// ////////////////////////- End writing to file: -// //////////////////////////////	
	
	public void initialize(){
		for( int i = 0; i < popSize; i++ ){
			population.add( new MultiLayerPerceptron( TransferFunctionType.SIGMOID, inLayerSize, hidLayerSize, outLayerSize ) );
			population.get(i).randomizeWeights(-1.0, 1.0);
			fitness.add(0.0);
		}
		calcFitness2();
		findBest();
		findAverage();
		bestFitness.add(currentBestFitness);
		averageFitness.add(currentAverageFitness);
		
	}
	
	public void initializeArray(){
		arraySize = inLayerSize * hidLayerSize + hidLayerSize * outLayerSize + hidLayerSize+ outLayerSize;// # of connections + bias
		populationArray = new double[popSize][arraySize+ 4*regionPerIndividual];	//space for 10 regions(start/end & x/y)
		bestArray = new double[arraySize+ 4*regionPerIndividual];


		
		for( int i = 0; i < popSize; i++ ){
			for ( int j = 0; j < arraySize; j++ ){
				populationArray[i][j] = rng.nextDouble()*2-1;
			}
			for( int k=0; k < (4*regionPerIndividual); k+=2){	// Setting up of x-axis followed by y-axis
				populationArray[i][arraySize+k] = rng.nextInt(num_cols); //correspond to cols variable in Robot.java
				populationArray[i][arraySize+k+1] = rng.nextInt(num_rows); //correspond to rows variable in Robot.java
			}
			fitness.add(0.0);
		}
		calcFitnessArray();
		findBestArray();
		findAverage();
		bestFitness.add(currentBestFitness);
		averageFitness.add(currentAverageFitness);
		if(distributed){
			Log.e("GA", "Dist started");
			elitePopulation = new double[eliteSize][arraySize];
			for(int i = 0; i < eliteSize; i++){
				eliteFitness.add(0.0);
			}
			distributionStarted = true;
		}
	}
	
	
public void runGenerationArray(){
		
		double[][] nextGen = new double[popSize][arraySize+ 4*regionPerIndividual];
		if(!distributed){
			if( hasBestArray ){
				copyArray(nextGen[0],bestArray, arraySize);
				copyArray(nextGen[1],bestArray, arraySize);
			}
			for( int i = 2; i < popSize; i++ ){
				double[] parent1 = new double[arraySize+ 4*regionPerIndividual],
						 parent2 = new double[arraySize+ 4*regionPerIndividual],
						 offspring = new double[arraySize+ 4*regionPerIndividual];
				parent1 = selectArray();
				parent2 = selectArray();
				offspring = crossoverArray(parent1, parent2);
				offspring = mutateArray(offspring);
				copyArray(nextGen[i],offspring, arraySize);
			}
			for( int i = 0; i < popSize; i++ ){
				copyArray(populationArray[i],nextGen[i], arraySize);
			}
		}else{
			int i = 0;
			if( hasBestArray ){
				copyArray(nextGen[0],bestArray, arraySize);
				i = 1;
			}
			for( ; i < popSize; i++ ){
				double[] parent1 = new double[arraySize],
						 parent2 = new double[arraySize],
						 offspring = new double[arraySize];
				parent1 = selectArray();
				parent2 = selectArray();
				offspring = crossoverArray(parent1, parent2);
				offspring = mutateArray(offspring);
				copyArray(nextGen[i],offspring, arraySize);
			}
			for( int j = 0; j < popSize; j++ ){
				copyArray(populationArray[j],nextGen[j], arraySize);
			}
		}
		
		calcFitnessArray();
		findBestArray();
		findAverage();
		bestFitness.add(currentBestFitness);
		averageFitness.add(currentAverageFitness);
	}
	
public double[] selectArray(){
	
	int position = -1;
	if( !distributed ){
		for( int i = 0; i < 3; i++ ){
			if( position == -1 ){
				position = rng.nextInt(popSize);
			}else{
				int challenger = rng.nextInt(popSize);
				if( fitness.get(challenger) < fitness.get(position) ){
					position = challenger;
				}
			}
		}
	}else{
		for( int i = 0; i < 3; i++ ){
			if( position == -1 ){
				if (elitesRecieved >0 ){
					position = rng.nextInt(popSize+eliteSize);
				}else{
					position = rng.nextInt(popSize);
				}
			}else{
				int challenger;
				if (elitesRecieved > 0 ){
					challenger = rng.nextInt(popSize+eliteSize);
				}else{
					challenger = rng.nextInt(popSize);
				}
				double challFit, posFit;
					if( position >= popSize ){
						Log.e("GA", " posi: " + position + " popS: " + popSize + " Chall: " + challenger 
								+ " efit: " + eliteFitness.size() + " epop: " + elitePopulation.length );
						posFit = eliteFitness.get(position - popSize);
					}else{
						posFit = fitness.get(position);
					}
					if( challenger >= popSize ){
						Log.e("GA", " posi: " + position + " popS: " + popSize + " Chall: " + challenger 
								+ " efit: " + eliteFitness.size() + " epop: " + elitePopulation.length );
						
						challFit = eliteFitness.get(challenger - popSize);
					}else{
						challFit = fitness.get(challenger);
					}
				
				
				
				if( challFit < posFit ){
					position = challenger;
				}
			}
		}
	}
	
	if ( position >= popSize ){
		return elitePopulation[position-popSize];
	}else{
		return populationArray[position];
	}
	
}


public double[] crossoverArray(double[] parent1, double[] parent2){
	
	double[] offspring = new double[arraySize+4*regionPerIndividual];
	
	for( int i = 0; i < arraySize; i++ ){
		if( rng.nextBoolean() ){
			offspring[i] = parent1[i];
		}else{
			offspring[i] = parent2[i];
		}
	}
	
	for(int j=0; j<4*regionPerIndividual; j++){
		if( rng.nextBoolean() ){
			offspring[arraySize+j] = parent1[arraySize+j];
		}else{
			offspring[arraySize+j] = parent2[arraySize+j];
		}
	}
	
	return offspring;
}

public double[] mutateArray(double[] offspring){
	double mutAmount;
	double coordinateOffset = 0;
	for (int i = 0; i < arraySize; i++) {
		if ( rng.nextDouble() < mutationRate ){
			mutAmount = rng.nextDouble()*.2 -.1;
			offspring[i] += mutAmount;
		}
	}

	for(int j=0; j< 4*regionPerIndividual; j+=2){
//		do{
			// Mutate x-axis co-ordinate
			if ( rng.nextDouble() < mutationRate ){
				coordinateOffset = (double) rng.nextInt(10)-5.0;
				if( (offspring[arraySize+j]+coordinateOffset) >= 0 && (offspring[arraySize+j]+coordinateOffset) < num_cols ){
					offspring[arraySize+j] = offspring[arraySize+j]+coordinateOffset;
				}
			}
			
			// Mutate y-axis co-ordinate
			if ( rng.nextDouble() < mutationRate ){
				coordinateOffset = (double) rng.nextInt(10)-5.0;
				if( (offspring[arraySize+j+1]+coordinateOffset) >= 0 && (offspring[arraySize+j+1]+coordinateOffset) < num_rows ){
					offspring[arraySize+j+1] = offspring[arraySize+j+1]+coordinateOffset;
				}
			}
//		}while( (j <= ((4*regionPerIndividual)-4)) && ( ((offspring[arraySize+j] - offspring[arraySize+j+2]) ==0) || ((offspring[arraySize+j+1] - offspring[arraySize+j+3]) ==0) ) );
	}
	
	return offspring;
}


public void calcFitnessArray(){
	fitness.clear();
	int[] numMoves = calcMoves();
	//Log.e("New Fit", "" + numMoves[0] + " " + numMoves[1] + " " + numMoves[2]);
	NeuralNetwork currentNet = new MultiLayerPerceptron( TransferFunctionType.SIGMOID, inLayerSize, hidLayerSize, outLayerSize );
	
	double[] temp_inputToNN= new double[regionPerIndividual];
	int temp_inputToNNIndex = 0;
	int temp_x1, temp_y1, temp_x2, temp_y2;
	
	for( int i = 0; i < popSize; i++){	
		double fitVal = 0;
		copyWeightsToANN(currentNet, populationArray[i]);
		for( int j = 0; j < testCases.size(); j++ ){
			temp_inputToNNIndex = 0;
			for(int k=0; k < 4*regionPerIndividual; k+=4){
				temp_x1 = (int) populationArray[i][arraySize+k];
				temp_y1 = (int) populationArray[i][arraySize+k+1];
				temp_x2 = (int) populationArray[i][arraySize+k+2];
				temp_y2 = (int) populationArray[i][arraySize+k+3];
				temp_inputToNN[temp_inputToNNIndex]=compute_fitness(testCases.get(j), temp_x1, temp_y1, temp_x2, temp_y2);
				temp_inputToNNIndex++;
			}
			//currentNet.setInput(testCases.get(j));
			currentNet.setInput(temp_inputToNN);
			currentNet.calculate();
			double[] expected = ans.get(j);
			double[] actual = currentNet.getOutput();
			fitVal += calcValue(actual, expected, numMoves);
		
		}
		fitness.add(1 - fitVal/3);
	}
	
}


public double compute_fitness(double[] test_case, int x1, int y1, int x2, int y2){
	double result = 0.0;
	int item_count = 0;
	int start_index = Math.min(y1, y2) + Math.min(x1, x2)*num_rows; // correspond to rows variable in Robot.java	
	for(int i=0; i< Math.abs(x2-x1); i++){
		for(int j=0; j< Math.abs(y2-y1); j++){
			result += test_case[start_index+i*num_rows+j];
 			item_count++;
		}
	}
	
	return item_count == 0 ? 0 : (result/item_count);
}

public double[] evolved_regions_input_to_NN(double[] image_array, double[] best_array){
	double[] temp_inputToNN= new double[regionPerIndividual];
	int temp_inputToNNIndex = 0;
	int temp_x1, temp_y1, temp_x2, temp_y2;
	
//	for( int j = 0; j < 4*regionPerIndividual; j++ ){
		temp_inputToNNIndex = 0;
		for(int k=0; k < 4*regionPerIndividual; k+=4){
			temp_x1 = (int) best_array[arraySize+k];
			temp_y1 = (int) best_array[arraySize+k+1];
			temp_x2 = (int) best_array[arraySize+k+2];
			temp_y2 = (int) best_array[arraySize+k+3];
Log.e("input to nn", Integer.toString(temp_x1)+"  "+Integer.toString(temp_y1)+"  "+Integer.toString(temp_x2)+"  "+Integer.toString(temp_y2)+"  ");
			temp_inputToNN[temp_inputToNNIndex]=compute_fitness(image_array, temp_x1, temp_y1, temp_x2, temp_y2);
			temp_inputToNNIndex++;
		}
//	}
	
	return temp_inputToNN;
}


public void calcEliteFitnessArray(){
	//eliteFitness.clear();
	int[] numMoves = calcMoves();
	//Log.e("New Fit", "" + numMoves[0] + " " + numMoves[1] + " " + numMoves[2]);
	NeuralNetwork currentNet = new MultiLayerPerceptron( TransferFunctionType.SIGMOID, inLayerSize, hidLayerSize, outLayerSize );

	if( elitesRecieved > 0 ){
		for( int i = 0; i < eliteSize; i++){
			
			double fitVal = 0;
			copyWeightsToANN(currentNet, elitePopulation[i]);
			
			for( int j = 0; j < testCases.size(); j++ ){
				currentNet.setInput(testCases.get(j));
				currentNet.calculate();
				double[] expected = ans.get(j);
				double[] actual = currentNet.getOutput();
				fitVal += calcValue(actual, expected, numMoves);
			
			}
			eliteFitness.remove(i);
			eliteFitness.add(i, 1 - fitVal/3);//.add(1 - fitVal/3);
		}
	}
}

public void copyArray(double[] ar1, double[] ar2, int size){
	for ( int i = 0; i < size; i++ ){
		ar1[i] = ar2[i];
	}
	
	for(int j=0; j < 4*regionPerIndividual; j++){
		ar1[size+j] = ar2[size+j];
	}
}


public void copyWeightsToANN( NeuralNetwork net, double[] weights){
	List<Connection> netConnections = new ArrayList<Connection>();
	
	for (Layer layer : net.getLayers()) {
	
        for (Neuron neuron : layer.getNeurons()) {

            for (Connection connection : neuron.getInputConnections()) {
            	netConnections.add(connection);
            }
        }
    }
	if( netConnections.size() != arraySize ){
		Log.e("COPY WEIGHTS", "connections: " + netConnections.size() + " arraySize: " +arraySize);
	}else{
		for ( int i = 0; i < arraySize; i++ ){
			netConnections.get(i).getWeight().setValue(weights[i]);
		}
	}
}

public void findBestArray(){
	int bestPos = -1;
	//String msg = "";
	for ( int i = 0; i < fitness.size(); i++ ){
		//msg += " " + fitness.get(i);
		if( bestPos == -1 ){
			bestPos = i;
		}else{
			if( fitness.get(i) < fitness.get(bestPos)){
				bestPos = i;
			}
		}
	}
	//System.out.println(msg);
	copyArray(bestArray, populationArray[bestPos], arraySize);
	hasBestArray = true;
	currentBestFitness = fitness.get(bestPos);
	bestError = fitness.get(bestPos);
	best = new MultiLayerPerceptron( TransferFunctionType.SIGMOID, inLayerSize, hidLayerSize, outLayerSize );
	copyWeightsToANN(best, bestArray);
}




	
	public void runGeneration(){
		
		List<NeuralNetwork> nextGen = new ArrayList<NeuralNetwork>();
		if( hasBest() ){
			nextGen.add(copyNetwork(best));
			nextGen.add(copyNetwork(best));
		}
		while( nextGen.size() < popSize ){
			NeuralNetwork parent1, parent2, offspring;
			parent1 = select();
			parent2 = select();
			offspring = crossover(parent1, parent2);
			mutate(offspring);
			nextGen.add(copyNetwork(offspring));
		}
		population.clear();
		for( int i = 0; i < nextGen.size(); i++ ){
			population.add(copyNetwork(nextGen.get(i)));
		}
		calcFitness2();
		findBest();
		findAverage();
		bestFitness.add(currentBestFitness);
		averageFitness.add(currentAverageFitness);
	}
	
	public void findAverage() {
		double a = 0.0;
		for ( int i = 0; i < fitness.size(); i++ ){
			a += fitness.get(i);
		}
		currentAverageFitness = a / fitness.size();
		
	}


	public String printNet( NeuralNetwork myNet ){
		String net = "";
		for (Layer layer : myNet.getLayers()) {
			net += " NEW LAYER \n" + layer.getNeuronsCount();
            for (Neuron neuron : layer.getNeurons()) {
            	net += "\n";
                for (Connection connection : neuron.getInputConnections()) {
                	net += connection.getWeight().value + " ";
                }
                
            }
            
        }
		
		return net;
	}
	
	public NeuralNetwork crossover(NeuralNetwork parent1, NeuralNetwork parent2){
		
		NeuralNetwork offspring = new MultiLayerPerceptron( TransferFunctionType.SIGMOID, inLayerSize, hidLayerSize, outLayerSize );
		List<Connection> offspringconnections = new ArrayList<Connection>();
		List<Connection> parent1connections = new ArrayList<Connection>();
		List<Connection> parent2connections = new ArrayList<Connection>();
		
		for (Layer layer : offspring.getLayers()) {
            for (Neuron neuron : layer.getNeurons()) {

                for (Connection connection : neuron.getInputConnections()) {
                	offspringconnections.add(connection);
                }
            }
        }
		
		for (Layer layer : parent1.getLayers()) {
            for (Neuron neuron : layer.getNeurons()) {
                for (Connection connection : neuron.getInputConnections()) {
                	parent1connections.add(connection);
                }
            }
        }
		for (Layer layer : parent2.getLayers()) {
            for (Neuron neuron : layer.getNeurons()) {
                for (Connection connection : neuron.getInputConnections()) {
                	parent2connections.add(connection);
                }
            }
        }
		
		
		for( int i = 0; i < offspringconnections.size(); i++ ){
			if( rng.nextBoolean() ){
				offspringconnections.get(i).getWeight().setValue(parent1connections.get(i).getWeight().value);
			}else{
				offspringconnections.get(i).getWeight().setValue(parent2connections.get(i).getWeight().value);
			}
		}

		return offspring;
	}
	
	public void mutate(NeuralNetwork offspring){
		double mutAmount;
		
		for (Layer layer : offspring.getLayers()) {
            for (Neuron neuron : layer.getNeurons()) {
                for (Connection connection : neuron.getInputConnections()) {
                	if ( rng.nextDouble() < mutationRate ){
                		mutAmount = rng.nextDouble()*.2 -.1;
                		connection.getWeight().setValue(connection.getWeight().value + mutAmount);

                	}
                }
            }
        }
	}
	
	
	public NeuralNetwork select(){
		NeuralNetwork selected;
		int position = -1;
		//Log.e("IN SELECT", "POP SIZE: " + population.size() +"   "+"FIT SIZE: "+fitness.size());
		for( int i = 0; i < 3; i++ ){
			if( position == -1 ){
				position = rng.nextInt(popSize);
			}else{
				int challenger = rng.nextInt(popSize);
				if( fitness.get(challenger) < fitness.get(position) ){
					position = challenger;
				}
			}
		}
		
		selected = population.get(position);
		
		return selected;		
	}
	
	public NeuralNetwork copyNetwork( NeuralNetwork inNet ){
		
		NeuralNetwork copiedNet = new MultiLayerPerceptron( TransferFunctionType.SIGMOID, inLayerSize, hidLayerSize, outLayerSize );
		
		List<Connection> innetconnections = new ArrayList<Connection>();
		List<Connection> copiedconnections = new ArrayList<Connection>();
		
		for (Layer layer : inNet.getLayers()) {
            for (Neuron neuron : layer.getNeurons()) {
                for (Connection connection : neuron.getInputConnections()) {
                	innetconnections.add(connection);
                }
            }
        }
		
		for (Layer layer : copiedNet.getLayers()) {
            for (Neuron neuron : layer.getNeurons()) {
                for (Connection connection : neuron.getInputConnections()) {
                	copiedconnections.add(connection);
                }
            }
        }
		
		for( int i = 0; i < copiedconnections.size(); i++){
			copiedconnections.get(i).getWeight().value = innetconnections.get(i).getWeight().value;
		}
		
		
		return copiedNet;
	}
	public void calcFitness(){
		fitness.clear();

		for( int i = 0; i < population.size(); i++){
			
			double totalError = 0;
			NeuralNetwork currentNet = population.get(i);
			
			for( int j = 0; j < testCases.size(); j++ ){
				
				double[] testAnswer = ans.get(j);
				currentNet.setInput(testCases.get(j));
				currentNet.calculate();
				double[] currentOut = currentNet.getOutput();
				totalError += calcError( testAnswer, currentOut);
				
			}
			fitness.add(Math.sqrt(totalError)/testCases.size());
		}
		
	}
	
	public double calcError( double[] expected, double[] recieved ){
		double error = 0;
		for (int i = 0; i < outLayerSize; i++){
			error += Math.pow( (expected[i]-recieved[i]), 2);
		}
		return error;
	}
	
	public void addTestCase(double[] input, double[] output) {
		if (load_test_cases == 0) {
			testCases.add(input);
			ans.add(output);
			if (output[0] > output[1] && output[0] > output[2]) {
				left++;
			} else if (output[1] > output[0] && output[1] > output[2]) {
				forward++;
			} else if (output[2] > output[1] && output[2] > output[0]) {
				right++;
			}

			storage_file = "test_cases.txt";
			writeToFile(
					storage_file,
					Arrays.toString(input) + "\n" + Arrays.toString(output)+"\n");
		} else {
Log.e("GANetTrainer", "loading test cases....");
			try {
				File input_file = new File(
						Environment.getExternalStorageDirectory()
								+ "/test_case_write", "test_cases.txt");
				FileInputStream fileIS = new FileInputStream(input_file);
				BufferedReader buf = new BufferedReader(new InputStreamReader(fileIS));
				String readString = new String();
				// just reading each line and pass it on the debugger
				for (int i = 0; (readString = buf.readLine()) != null; i = i + 3) {
					// read input
					input = getValueFromFile(readString);
					testCases.add(input);
					// read output
					readString = buf.readLine();
					output = getValueFromFile(readString);
					ans.add(output);
					
					if (output[0] > output[1] && output[0] > output[2]) {
						left++;
					} else if (output[1] > output[0] && output[1] > output[2]) {
						forward++;
					} else if (output[2] > output[1] && output[2] > output[0]) {
						right++;
					}
				}
				buf.close();
				// Log.e("GANet - Read from file");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
Log.e("GANetTrainer", "loaded test cases....");
		}
	}
	
	private double[] getValueFromFile(String readString) {
		readString = readString.substring(1, readString.length() - 1);
		String[] strArray = readString.split(",");
		List<Double> inputArray = new LinkedList<Double>();
		for (String str : strArray) {
			inputArray.add(Double.parseDouble(str.trim()));
		}

		double[] target = new double[inputArray.size()];
		for (int i = 0; i < target.length; i++) {
			target[i] = inputArray.get(i); // java 1.5+ style (outboxing)
		}

		return target;
	}
	
	
	public void calcFitness2(){
		fitness.clear();
		int[] numMoves = calcMoves();
		//Log.e("New Fit", "" + numMoves[0] + " " + numMoves[1] + " " + numMoves[2]);
		for( int i = 0; i < population.size(); i++){
			
			double fitVal = 0;
			NeuralNetwork currentNet = population.get(i);
			
			for( int j = 0; j < testCases.size(); j++ ){
				currentNet.setInput(testCases.get(j));
				currentNet.calculate();
				double[] expected = ans.get(j);
				double[] actual = currentNet.getOutput();
				fitVal += calcValue(actual, expected, numMoves);
			
			}
			fitness.add(1 - fitVal/3);
		}
		
		
	}
	
	
	public int[] calcMoves(){
		int[] moves = new int[3];
		for ( int i = 0; i < ans.size(); i++ ){
			double[] answer = ans.get(i);
			moves[getWinner(answer)]++;
		}
		return moves;
		
	}
	public int getWinner(double[] values){
		int winner = 0;
		if( values[0] > values[1] && values[0] > values[2] ){
			winner = 0;
		}else if( values[1] > values[0] && values[1] > values[2] ){
			winner = 1;
		}
		else if( values[2] > values[1] && values[2] > values[0] ){
			winner = 2;
		}
		return winner;
	}
	
	public double calcValue(double[] actual, double[] expected, int[] moves){
		double value = 0.0;
		double diff1 = 0.0, diff2 = 0.0;
		
		int expWin = getWinner(expected);
		double expectVal = actual[expWin];
		
		if( expWin == 0 ){
			diff1 = getDiff(expectVal, actual[1]);
			diff2 = getDiff(expectVal, actual[2]);
		}else if( expWin == 1 ){
			diff1 = getDiff(expectVal, actual[0]);
			diff2 = getDiff(expectVal, actual[2]);
		}else if( expWin == 2 ){
			diff1 = getDiff(expectVal, actual[0]);
			diff2 = getDiff(expectVal, actual[1]);
		}
		value += calcPoints( diff1, moves[getWinner(expected)]);
		//Log.e("New Fit", " Diff1 " + diff1 + " " + value);
		value += calcPoints( diff2, moves[getWinner(expected)]);
		
		//Log.e("New Fit", " Diff2 " + diff2 + " " + value);
		return value;
	}
	
	public double calcPoints( double diff, int numMove){
		double points = 0.0;
		
		if( diff >= fitThresh){
			points += .5/numMove;
		}else{
			points += (.5*(diff/fitThresh))/numMove;
		}
		
		
		return points;
		
		
	}
	
	public double getDiff( double expectVal, double actVal ){
		double difference = 0.0;
			difference = expectVal - actVal;
		return difference;
	}
	
	public int getNumTestcases(){
		return testCases.size();
	}
	
	public void findBest(){
		int bestPos = -1;
		//String msg = "";
		for ( int i = 0; i < fitness.size(); i++ ){
			//msg += " " + fitness.get(i);
			if( bestPos == -1 ){
				bestPos = i;
			}else{
				if( fitness.get(i) < fitness.get(bestPos)){
					bestPos = i;
				}
			}
		}
		//System.out.println(msg);
		best = copyNetwork(population.get(bestPos));
		currentBestFitness = fitness.get(bestPos);
		bestError = fitness.get(bestPos);
	}
	
	public boolean hasBest(){
		if( best != null ){
			return true;
		}
		else{
			return false;
		}
	}
	public NeuralNetwork getBest(){
		return best;
	}


	public void setUpDistrobuted(int numberOfDistBots) {
		Log.e("GA", "Dist setup");
		eliteSize = numberOfDistBots;
		distributed = true;
	}

	public void updateElites(double [][] elites){
		
		if (distributionStarted){
			Log.e("GA", "Elites Recieved");
			elitesRecieved++;
			for( int i = 0; i < eliteSize; i++){
				copyArray(elitePopulation[i], elites[i], arraySize);
			}
			calcEliteFitnessArray();
		}
	}

	public List<Double> getAverageList() {
		return averageFitness;
	}


	public List<Double> getBestList() {
		return bestFitness;
	}


	public double getBestFitness() {
		return currentBestFitness;
	}


	public double getAverageFitness() {
		return currentAverageFitness;
	}


	

}