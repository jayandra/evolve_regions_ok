package cotsbots.robot.robot;

import android.app.Activity;
import android.util.Log;

import ui.lair.imaging.ImageProcessor;
import ui.lair.roadClassification.RoadClassifier;

import core.NeuralNetwork;

import cotsbots.graduate.robotcontroller.RobotController;
import cotsbots.graduate.robotcontroller.RobotController.SteeringType;
import cotsbots.robot.data.RobotData;
import cotsbots.robot.data.RobotData.state;




public class Robot extends Thread{
	
	
	
	public SteeringType driveType;
	public RobotController robotControl;
	public RobotData myData;
	
	public state currentState = state.CLEAR;
	
	
	public NeuralNetwork ann;
	public double[] best_array;
	public int inputs = 8, hidden = 5, outputs = 3, popSize = 75;
	public GANetTrainer ga;
	public EvolveThread evolver;
	public boolean evolving = false;
	public boolean offline = false;
	
	public boolean running = true;
	public boolean paused = false;
	public RobotView myRobotView;
	
	
	public RoadClassifier imageProcessor;
	private int modelType = ImageProcessor.MODEL_TYPE_BACKPROJECT;
	public int startx = 315, endx = 365, starty = 400, endy = 450;
	public int col = 32, rows = 24;
	//double[] output = imageProcessor.calc(m_Rgba,col,rows);
	//imageProcessor.updateModel(m_Rgba);
	public double[] currentMove = new double[3];
	public Activity myActivity;
	
	
	public Robot(String ip, SteeringType drive, RobotData data, Activity c){
		imageProcessor = ImageProcessor.getRoadClassifier(modelType, startx, starty, endx, endy);
		ga = new GANetTrainer(popSize, inputs, hidden, outputs);
		driveType = drive;
		myData = data;
		myActivity = c;
		Log.e("Robot Class", "Robot Started");
		startRobot(ip);
		startRobotView(c);
	}
	
	
	public RobotView getRobotView(){
		return myRobotView;
	}
	
	public void startEvolution(){
		evolving = true;
		evolver.start();
	}
	public void changeState( state newState ){
		
		if( newState == state.ANNTRAINING ){
			if(myData.evoReady && !evolving){
				currentState = newState;
				if(!offline){
					startEvolution();
				}
				myRobotView.overlayOn = true;
			}
		}else if( newState == state.AUTONOMOUS ){
			if (evolver.done ){
				myRobotView.overlayOn = true;
				currentState = newState;
				ann = evolver.getBest();
				best_array = evolver.bestArray;
			}
		}else{
			myRobotView.overlayOn = false;
			currentState = newState;
		}
		
	}
	public void pause(){
		if(paused){
			paused = false;
		}else{
			paused = true;
		}
	}
	
	public void setupEvolution(){
		evolver = new EvolveThread(ga, myData, myActivity);
		offline = myData.Conditions.offline;
	}
	
	public void drive( int direction ){
		// every move:
		// If histogram training: update road model, move
		// If Training ANN create Training Case, move
		// otherwise: move
		
		currentMove = new double[3];
		
		if( currentState == state.VISIONTRAINING ){
		
			imageProcessor.updateModel(myRobotView.getView());
			
		}else if( currentState == state.ANNTRAINING ){
			switch( direction ){
			case 1: // forward
				currentMove[0] = 0;
				currentMove[1] = 1;
				currentMove[2] = 0;
				break;
			case 3: // left
				currentMove[0] = 1;
				currentMove[1] = 0;
				currentMove[2] = 0;
				break;
			case 4: // right
				currentMove[0] = 0;
				currentMove[1] = 0;
				currentMove[2] = 1;
				break;
			}
			//Log.e("Robot Class", "Before Calc");
			ga.addTestCase(imageProcessor.calc(myRobotView.getView(), col, rows), currentMove);
			//Log.e("Robot Class", "After Calc");
		}
		
		switch( direction ){
		case 0: // stop
			robotControl.driveVehicle(.5, .5, driveType);
			break;
		case 1: // forward
			robotControl.driveVehicle(.5, 1, driveType);
		
			break;
		case 2: // reverse
			robotControl.driveVehicle(.5, 0, driveType);
			break;
		case 3: // left
			robotControl.driveVehicle(0, 1, driveType);
			break;
		case 4: // right
			robotControl.driveVehicle(1, 1, driveType);
			break;
		}
		
	}
	
	
	public void driveByANN(){
//		ann.setInput(imageProcessor.calc(myRobotView.getView() ,col,rows) );
		double[] image_array = imageProcessor.calc(myRobotView.getView() ,col,rows);
		ann.setInput(ga.evolved_regions_input_to_NN(image_array, best_array) );

		ann.calculate();
		double[] output = ann.getOutput();
		if( output[0] > output[1] && output[0] > output[2]){
			drive(3); //left
		}else if( output[1] > output[0] && output[1] > output[2]){
			drive(1); //forward
		}else if( output[2] > output[0] && output[2] > output[1]){
			drive(4); //right
		}else{
			Log.e("AUTO DRIVE", "Tie");
		}
		
	}
	
	
	public void startRobot(String mac){
		robotControl = new RobotController(mac);
		
		while(!robotControl.isConnected()){
			;
		}
		Log.e("Robot Class", "Robot Controller Connected");
	}
	public void startRobotView(Activity c){
		myRobotView = new RobotView(c, imageProcessor);
	}
	
	
	public void run(){
		while(running){
			
			/*
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				
			}
			*/
			
			
			//every round: update robot data
			//if evolving: update evolution data
			//if autonomous: process image, and make movement decision
			//myData.currentState = currentState;
			if( !paused ){
				if ( currentState == state.AUTONOMOUS ){
					driveByANN();
					
				}
			}
			
			
		}
		
		
		
		
		
	}
	
	
	
	
}
