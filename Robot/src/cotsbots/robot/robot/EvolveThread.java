package cotsbots.robot.robot;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

import android.util.Log;
import core.NeuralNetwork;
import cotsbots.robot.data.EvolutionConditions;
import cotsbots.robot.data.RobotData;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Environment;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;

public class EvolveThread extends Thread implements TextToSpeech.OnInitListener{

	public GANetTrainer trainer;
	public boolean done = false;
	public double fitnessThresh = .2;
	public EvolutionConditions conditions;
	public int maxGen = 100;
	public int startCondition;
	public RobotData myData;
	public NeuralNetwork best;
	public TextToSpeech tts;
	public Activity thisAct;
	public boolean offline = false;
	public boolean distributed = false;
	public int currentGeneration = 0;
	public double[] bestArray;

	////////////////////////// - For writing to file in line:210 - //////////////////////////////
	File storage_path;
	String storage_file = "";
	private File ensure() {
		storage_path = new File( Environment.getExternalStorageDirectory()+ "/test_file_write");
		boolean success = true;
		if (!storage_path.exists()) {
		    success = storage_path.mkdir();
		    if (!success) {
		    	Log.e("EvolveThread", "Error creating test_file_write folder");
			}
		}
		return storage_path;
	}
	private void writeToFile(String filename, String content){
		File file = new File(ensure(),filename);
		if(!file.exists()) {
			try {
				file.createNewFile();
			} catch (Exception e) {
				e.printStackTrace();
				Log.e("EvolveThread", "Error creating file");
			}
		}
		try {
			FileWriter fw = new FileWriter(file, true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(content);
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
			Log.e("EvolveThread", "Error writing file");
		}
	}
	//////////////////////////- End writing to file: - //////////////////////////////
	
	
	public EvolveThread(GANetTrainer t, RobotData r, Activity c){
		trainer = t;
		myData = r;
		thisAct = c;
		conditions = myData.Conditions;
		setup();
		setupttt();

	}
	
	public void setupttt(){
		tts = new TextToSpeech(thisAct, this);
		
	}
	
	public void setup(){
		maxGen = conditions.maxGenerations;
		if (conditions.distributed){
			trainer.setUpDistrobuted(conditions.numberOfDistBots);
			distributed = true;
		}
		startCondition = conditions.startCondition;
	}
	
	public void updateElites( double [] [] elites ){
		trainer.updateElites( elites );
	}
	
	public NeuralNetwork getBest(){
		return best;
	}
	
	public void run(){
		
		
		boolean started = false;
		int currentTC = 0, oldTC = 0;
		while( currentGeneration < maxGen ){
			currentTC = trainer.testCases.size();
			 
			if(offline && !started){
				trainer.initializeArray();
				started = true;
				currentGeneration++;
				tts.speak("Evolution Started", TextToSpeech.QUEUE_FLUSH, null);
				if(distributed){
					try{
						Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION) ;
						Ringtone r = RingtoneManager.getRingtone(thisAct, notification);
						r.play();
					}
					catch (Exception e){}
				}
			}else{
				if ( !started ){
					if ( startCondition == 0){
						if( trainer.testCases.size() > 0 ){
							trainer.initializeArray();
							started = true;
							currentGeneration++;
							tts.speak("Evolution Started", TextToSpeech.QUEUE_FLUSH, null);
							if(distributed){
								try{
									Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION) ;
									Ringtone r = RingtoneManager.getRingtone(thisAct, notification);
									r.play();
								}
								catch (Exception e){}
							}
						}
					}
					else if ( startCondition == 1){
						if( trainer.left > 0 && trainer.right > 0 && trainer.forward > 0 ){
							trainer.initializeArray();
							started = true;
							currentGeneration++;
							tts.speak("Evolution Started", TextToSpeech.QUEUE_FLUSH, null);
							if(distributed){
								try{
									Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION) ;
									Ringtone r = RingtoneManager.getRingtone(thisAct, notification);
									r.play();
								}
								catch (Exception e){}
							}
						}
					}
					else if ( startCondition == 2){
						if( trainer.left > 2 && trainer.right > 2 && trainer.forward > 2 ){
							// Sleep 20 seconds so that GANetTrainer.addTestCase gets time to load test cases from file
							try {
							    Thread.sleep(20000);
							} catch(InterruptedException ex) {
							    Thread.currentThread().interrupt();
							}
							trainer.initializeArray();
							started = true;
							currentGeneration++;
							tts.speak("Evolution Started", TextToSpeech.QUEUE_FLUSH, null);
							if(distributed){
								try{
									Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION) ;
									Ringtone r = RingtoneManager.getRingtone(thisAct, notification);
									r.play();
								}
								catch (Exception e){}
							}
						}
					}
					else if ( startCondition == 3){
						if( trainer.left > 9 && trainer.right > 9 && trainer.forward > 9 ){
							// Sleep 20 seconds so that GANetTrainer.addTestCase gets time to load test cases from file
							try {
							    Thread.sleep(20000);
							} catch(InterruptedException ex) {
							    Thread.currentThread().interrupt();
							}
							trainer.initializeArray();
							started = true;
							currentGeneration++;
							tts.speak("Evolution Started", TextToSpeech.QUEUE_FLUSH, null);
							if(distributed){
								try{
									Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION) ;
									Ringtone r = RingtoneManager.getRingtone(thisAct, notification);
									r.play();
								}
								catch (Exception e){}
							}
						}
					}
					 
					oldTC = trainer.testCases.size();
					
				}
			}
			
			
			
			if( started /*&& currentTC > oldTC*/){
				trainer.runGenerationArray();
storage_file = "EvolveThread.txt";
// Write the bestArray to a file with a time stamp and fitness value <<<====
// time trainer.currentBestFitness currentGeneration trainer.bestArray
writeToFile(storage_file,
			new Date().toString() + "    "+
			Integer.toString(currentGeneration) + "    "+
			trainer.currentBestFitness + "    "+ trainer.currentAverageFitness+ "    "+
			"left - "+Integer.toString(trainer.left)+"  right - "+Integer.toString(trainer.right)+"  for- "+Integer.toString(trainer.forward)+"   "+
			Arrays.toString(trainer.bestArray)+"\n"
		   );
				myData.evoData.update(trainer.getAverageList(), trainer.getBestList(), currentGeneration, 
									  trainer.getBestFitness(), trainer.getAverageFitness(), trainer.getNumTestcases(),
									  trainer.forward, trainer.left, trainer.right, trainer.elitesRecieved);
				
				currentGeneration++;
Log.e("EvolveThread","current generation is ----------------- "+Integer.toString(currentGeneration));
				best = trainer.getBest();
				oldTC = trainer.testCases.size();
				
				if(currentGeneration %25 == 0){
					tts.speak(""+currentGeneration, TextToSpeech.QUEUE_FLUSH, null);
					if(distributed){
						try{
							Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION) ;
							Ringtone r = RingtoneManager.getRingtone(thisAct, notification);
							r.play();
						}
						catch (Exception e){}
					}
				}
				//Log.e("Evolver", "Gen: " +currentGeneration + " BestFit: " + trainer.getBestFitness() );
			}
			bestArray = trainer.bestArray;
			

			
		}
		done = true;
		Log.e("Evolver", "GA Complete. Average list size: " + trainer.getAverageList().size() + " Best List Size: " + trainer.getBestList().size() + 
				" best fitness: " + trainer.getBestFitness() );
		tts.speak("Evolution Done", TextToSpeech.QUEUE_FLUSH, null);
		if(distributed){
			try{
				Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION) ;
				Ringtone r = RingtoneManager.getRingtone(thisAct, notification);
				r.play();
				r.play();
			}
			catch (Exception e){}
		}
	}

	@Override
	public void onInit(int status) {
		// TODO Auto-generated method stub
		if (status == TextToSpeech.SUCCESS) {
			 
            int result = tts.setLanguage(Locale.US);
		}
	}
}
